from pptx import Presentation
from pptx.util import Inches

import pandas as pd

from PIL import Image
import os

# pip install python-pttx
# thanks to Thomas Winters for this
def _add_text(slide, placeholder_id, _text):
    placeholder = slide.placeholders[placeholder_id]
    placeholder.text = _text
def _add_image(slide, placeholder_id, image_url):
    placeholder = slide.placeholders[placeholder_id]
 
    # Calculate the image size of the image
    im = Image.open(image_url)
    width, height = im.size
 
    # Make sure the placeholder doesn't zoom in
    placeholder.height = height
    placeholder.width = width
 
    # Insert the picture
    placeholder = placeholder.insert_picture(image_url)
 
    # Calculate ratios and compare
    image_ratio = width / height
    placeholder_ratio = placeholder.width / placeholder.height
    ratio_difference = placeholder_ratio - image_ratio
 
    # Placeholder width too wide:
    if ratio_difference > 0:
        difference_on_each_side = ratio_difference / 2
        placeholder.crop_left = -difference_on_each_side
        placeholder.crop_right = -difference_on_each_side
    # Placeholder height too high
    else:
        difference_on_each_side = -ratio_difference / 2
        placeholder.crop_bottom = -difference_on_each_side
        placeholder.crop_top = -difference_on_each_side


table = pd.read_excel("test.xlsx")
cols = table.columns
conv = dict(zip(cols ,[str] * len(cols)))
table = pd.read_excel("test.xlsx", converters=conv)
a = table["a"].tolist()
b = table["b"].tolist()
c = table["c"].tolist()
d = table["d"].tolist()

temp = list()
j = 0
k = 0
l = 0
for item in c:
    if j == 0:
        j = item
        k = list()
        k.append([item,b[l],d[l],a[l]])
    elif j != item:
        temp.append(k)
        k = list()
        k.append([item,b[l],d[l],a[l]])
    elif j == item:
        k.append([item,b[l],d[l],a[l]])
    j = item
    l += 1
temp.append(k)
print (temp)

prs = Presentation('test.pptx')
# set width and height to 16 and 9 inches.
# prs.slide_width = Inches(16)
# prs.slide_height = Inches(9)

# imgpath = "จ.ส.อ. กิตติพัฒน์  ปอโนนสูง.jpg"
left = Inches(1)
top = Inches(1)

title_slide_layout = prs.slide_layouts[8]


npic = 4
pathImg = "X:/Image/"
for g in temp:
    m = 0
    for h in g:
        if os.path.exists(pathImg+h[1]+".jpg") and os.path.getsize(pathImg+h[1]+".jpg") > 0 :
            newImg = pathImg+h[1]+".jpg"
        else:
            newImg = pathImg+"person.jpg"

        if m % npic == 0:
            print ("add slide ",m)
            slide = prs.slides.add_slide(title_slide_layout)
            # for shape in slide.shapes:
            #     if shape.is_placeholder:
            #         phf = shape.placeholder_format
            #         print('%d, %s' % (phf.idx, phf.type))
            
            print ("add placeolder")
            _add_text(slide,0,h[0])
            _add_image(slide,1,newImg)
            _add_text(slide,15,h[3]+". "+h[1])
            _add_text(slide,2,h[2])
 
        elif m % npic == 1:
            print ("add placeolder ",m)
            _add_image(slide,13,newImg)
            _add_text(slide,16,h[3]+". "+h[1])
            _add_text(slide,14,h[2])
        
        elif m % npic == 2:
            print ("add placeolder ",m)
            _add_image(slide,17,newImg)
            _add_text(slide,19,h[3]+". "+h[1])
            _add_text(slide,18,h[2])

        elif m % npic == 3:
            print ("add placeolder ",m)
            _add_image(slide,20,newImg)
            _add_text(slide,22,h[3]+". "+h[1])
            _add_text(slide,21,h[2])

        m += 1
        print (h)

prs.save('test1.pptx')

# for item in c:
#     if i%3  == 0 :
#         print ("add slide %d",i)
#     elif i%3 < 3 :
#         print ("add placeholder %d",i)
#     i += 1

    